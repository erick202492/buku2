var model = require("../models/index");

module.exports = function (app) {
    /* GET Books listing. */
    app.get("/books", function (req, res, next) {
        model.Books.findAll({})
            .then(books =>
                res.json({
                    error: false,
                    data: books
                })
            )
            .catch(error =>
                res.json({
                    error: true,
                    data: [],
                    error: error
                })
            );
    });

    app.get("/books/:id", function (req, res, next) {
        
        model.Books.findAll({
            where: {
                id: req.params.id
              }
        })
            .then(books =>
                res.json({
                    error: false,
                    data: books
                })
            )
            .catch(error =>
                res.json({
                    error: true,
                    data: [],
                    error: error
                })
            );
    });



    /* POST Books. */
    app.post("/books", function (req, res, next) {
        const { author,published_date,pages,language,publisher_id } = req.body;
        model.Books.create({
            author:author,
            published_date:published_date,
            pages:pages,
            language:language,
            publisher_id:publisher_id
        })
            .then(Books =>
                res.status(201).json({
                    error: false,
                    data: Books,
                    message: "New Books has been created."
                })
            )
            .catch(error =>
                res.json({
                    error: true,
                    data: [],
                    error: error
                })
            );
    });

    /* update Books. */
    app.put("/books/:id", function (req, res, next) {
        const Books_id = req.params.id;

        const { author,published_date,pages,language,publisher_id } = req.body;

        model.Books.update(
            {
                author:author,
                published_date:published_date,
                pages:pages,
                language:language,
                publisher_id:publisher_id
            },
            {
                where: {
                    id: Books_id
                }
            }
        )
            .then(Books =>
                res.json({
                    error: false,
                    message: "Books has been updated."
                })
            )
            .catch(error =>
                res.json({
                    error: true,
                    error: error
                })
            );
    });

    /* GET Books listing. */
    /* Delete Books. */
    app.delete("books/:id", function (req, res, next) {
        const Books_id = req.params.id;

        model.Books.destroy({
            where: {
                id: Books_id
            }
        })
            .then(status =>
                res.json({
                    error: false,
                    message: "Books has been delete."
                })
            )
            .catch(error =>
                res.json({
                    error: true,
                    error: error
                })
            );
    });
};